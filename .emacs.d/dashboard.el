;; Config
(setq dashboard-startup-banner t)
(setq dashboard-show-shortcuts 'nil)
(dashboard-setup-startup-hook)
(setq initial-buffer-choice (lambda () (get-buffer "*dashboard*")))

(setq dashboard-items
      '((bookmarks . 7)
      (recents . 7)
      (projects . 2)
      (agenda . 2)))

;; Quick keys
(dashboard-insert-shortcut (dashboard-get-shortcut 'recents) "f" "Recent Files:")
(dashboard-insert-shortcut (dashboard-get-shortuct 'agenda) "a" "Agenda for today:")
(define-key dashboard-mode-map (kbd "n") 'next-line)
(define-key dashboard-mode-map (kbd "p") 'previous-line)

(defun my/new-frame-dashboard ()
  "Create a new frame showing the dashboard"
  (interactive)
  (display-buffer "*dashboard*" '(display-buffer-pop-up-frame . nil)))
(global-set-key (kbd "C-c n") #'my/new-frame-dashboard)

(defun my/switch-to-dashboard ()
  "Switch to the dashboard buffer"
  (interactive)
  (switch-to-buffer "*dashboard*"))
(global-set-key (kbd "C-c d") #'my/switch-to-dashboard)


;; set title
(setq dashboard-banner-logo-title "Welcome barry!")
;; set banner
(setq dashboard-startup-banner "~/Pictures/barrybbenson.gif")
;; Center content
(setq dashboard-center-content t)
;; add icons
(setq dashboard-set-heading-icons t)
(setq dashboard-set-file-icons t)
;; show navigator below banner
(setq dashboard-set-navigator t)
