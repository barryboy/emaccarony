;; lower gc calls
(setq gc-cons-threshold (* 50 1024 1024))
;; profile emacs startup
(add-hook 'emacs-startup-hook
	  (lambda ()
	    (message "*** Emacs loaded in %s seconds with %d garbage collections" (emacs-init-time "%.2f") gcs-done)))

;; package manager
(require 'package)
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
             ("org" . "https://orgmode.org/elpa/")
	     ("elpa" . "https://elpa.gnu.org/packages/")))
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))


;; use-package
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

;; autosave
(setq backup-directory-alist '(("." . "~/.emacs.d/backup"))
      backup-by-copying t    ; Don't delink hardlinks
      version-control t      ; Use version numbers on backups
      delete-old-versions t  ; Automatically delete excess backups
      kept-new-versions 20   ; How many of the newest versions to keep
      kept-old-versions 5)   ; And how many of the old

;; set fonts
(set-face-attribute 'default nil :font "Source Code Pro" :height 120)
(set-face-attribute 'fixed-pitch nil :font "Source Code Pro" :height 120)
(set-face-attribute 'variable-pitch nil :font "Source Code Pro" :height 140)
(set-frame-font "Source Code Pro 12")
(setq default-frame-alist '((font . "Source Code Pro 12")))

;; disable toolbars
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)
(setq frame-resize-pixelwise t)

;; set modeline
(setq doom-modeline-support-imenu t)
(setq doom-modeline-height 25)
(use-package doom-modeline
  :ensure t
  :init
    (doom-modeline-mode 1)
  :custom ((doom-modeline-height 25)))

; set line number
(use-package linum-relative
  :ensure t
  :config
  (linum-relative-global-mode 1)
  (setq linum-relative-current-symbol "")
  (setq linum-relative-format "%4s \u2502"))

(defun bb/absolute-line-number ()
  (if (and (not (minibufferp))
           (eq (line-number-at-pos) linum-relative-last-pos))
      (format "%4s \u2502" (line-number-at-pos)) ""))

(add-hook 'linum-before-numbering-hook #'bb/absolute-line-number)

(add-hook 'dashboard-after-initialize-hook
          (lambda ()
            (with-current-buffer "*dashboard*"
              (linum-relative-mode -1))))

;; Trust all themes, unsecure but you know what ur doing
(setq custom-safe-themes t)
;; set theme
(use-package doom-themes
  :ensure t
  :config
  (setq doom-themes-enable-bold t)
  (setq	doom-themes-enable-italic t)
  (load-theme 'doom-laserwave)
  ;(load-theme 'doom-shades-of-purple)
)

;; set icons
(use-package all-the-icons
  :ensure t)

;; emojis
(use-package emojify
  :ensure t
  :defer 0)

;; set transparency
(set-frame-parameter (selected-frame) 'alpha '(90 . 85))
(add-to-list 'default-frame-alist '(alpha . (90 . 85)))
(set-frame-parameter (selected-frame) 'fullscreen 'maximized)
(add-to-list 'default-frame-alist '(fullscreen . maximized))

(if (daemonp)
    (add-hook 'after-make-frame-functions
              (lambda (frame)
                (with-selected-frame frame
                  (when (display-graphic-p)
                    (use-package dashboard
                      :ensure t
                      :config
                      (dashboard-setup-startup-hook)
                      (setq dashboard-items '((bookmarks . 10)
                                              (recents . 5)
                                              (projects . 2)
                                              (agenda . 2)))
                      (setq dashboard-banner-logo-title "Welcome Barry!")
                      (setq dashboard-startup-banner "~/Pictures/barrybbenson.gif")
                      (setq dashboard-center-content t)
                      (setq dashboard-show-shortcuts t)
                      (setq initial-buffer-choice
                            (lambda ()
                              (if (get-buffer "*dashboard*")
                                  (get-buffer "*dashboard*")
                                (progn
                                  (dashboard-refresh-buffer)
                                  (get-buffer "*dashboard*")))))
                      (setq dashboard-set-heading-icons t)
                      (setq dashboard-set-navigator t)
                      (dashboard-insert-shortcut (dashboard-get-shortcut 'recents) "f" "Recent Files:")
                      (dashboard-insert-shortcut (dashboard-get-shortcut 'agenda) "a" "Agenda for today:"))))))
  ;; For non-daemon mode, set up normally
  (use-package dashboard
    :ensure t
    :config
    (dashboard-setup-startup-hook)
    (setq dashboard-items '((bookmarks . 10)
                            (recents . 5)
                            (projects . 2)
                            (agenda . 2)))
    (setq dashboard-banner-logo-title "Welcome Barry!")
    (setq dashboard-startup-banner "~/Pictures/barrybbenson.gif")
    (setq dashboard-center-content t)
    (setq dashboard-show-shortcuts t)
    (setq initial-buffer-choice
          (lambda ()
            (if (get-buffer "*dashboard*")
                (get-buffer "*dashboard*")
              (progn
                (dashboard-refresh-buffer)
                (get-buffer "*dashboard*")))))
    (setq dashboard-set-heading-icons t)
    (setq dashboard-set-navigator t)
    (dashboard-insert-shortcut (dashboard-get-shortcut 'recents) "f" "Recent Files:")
    (dashboard-insert-shortcut (dashboard-get-shortcut 'agenda) "a" "Agenda for today:")))

;; set evil
(use-package evil
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  (setq evil-want-C-i-jump nil)
  :config
  (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join))

(evil-global-set-key 'motion "j" 'evil-next-visual-line)
(evil-global-set-key 'motion "k" 'evil-previous-visual-line)
(evil-set-initial-state 'messages-buffer-mode 'normal)
(evil-set-initial-state 'dashboard-mode 'normal)
(evil-set-undo-system 'undo-redo)
(define-key evil-normal-state-map (kbd "C-h") 'evil-window-left)
(define-key evil-normal-state-map (kbd "C-j") 'evil-window-down)
(define-key evil-normal-state-map (kbd "C-k") 'evil-window-up)
(define-key evil-normal-state-map (kbd "C-l") 'evil-window-right)

(use-package evil-collection
  :ensure t
  :after evil
  :config (evil-collection-init))

;; small misc
(save-place-mode 1)
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)
(defalias 'yes-or-no-p 'y-or-n-p)
(use-package hindent :ensure t :defer 0)
(use-package simpleclip :ensure t :config (simpleclip-mode 1))
(use-package crux :ensure t :bind (("C-a" . crux-move-beginning-of-line)))
(add-hook 'before-save-hook 'delete-trailing-whitespace)
(use-package savehist :ensure t :init (savehist-mode))
;; general bindings
(global-set-key (kbd "C-<tab>") 'other-window)

;; set parenthesis behaviour
(use-package smartparens
  :ensure t
  :config (add-hook 'prog-mode-hook 'smartparens-mode))

(bind-keys
   :map smartparens-mode-map
     ("C-<down>" . sp-down-sexp)
     ("C-<up>" . sp-up-sexp))

(use-package rainbow-delimiters
  :ensure t)

;; set minibuf autocomplete
(use-package vertico
  :ensure t
  :bind (:map vertico-map
	      ("C-j" . vertico-next)
	      ("C-k" . vertico-previous)
	      ("C-f" . vertico-exit)
	      :map minibuffer-local-map
	      ("M-h" . backward-kill-word))
  :custom (vertico-cycle t)
  :init (vertico-mode))

(define-key vertico-map "?" #'minibuffer-completion-help) ; autocomplete with tab
(define-key vertico-map (kbd "M-RET") #'minibuffer-force-complete-and-exit)
(define-key vertico-map (kbd "M-TAB") #'minibuffer-complete)

(use-package marginalia
  :after vertico
  :ensure t
  :bind (("M-A" . marginalia-cycle)
	 :map minibuffer-local-map
	 ("M-A" . marginalia-cycle))
  :init (marginalia-mode))

(use-package orderless
  :ensure t
  :custom
  (completion-styles '(orderless basic))
  (completion-category-overrides '((file (style basic partial-complete)))))

;; set recent file mode
(recentf-mode 1)
(setq recentf-max-menu-items 20)
(setq recent-max-saved-items 20)
(run-at-time nil (* 5 60) 'recentf-save-list)

(defun recentf-open-files-compl ()
  (interactive)
  (let* ((tocpl (mapcar (lambda (x) (cons (file-name-nondirectory x) x))
			recentf-list))
	 (fname (completing-read "File name: " tocpl nil nil)))
    (when fname (find-file (cdr (assoc-string fname tocpl))))))

;; set magit git client
(use-package magit
  :defer 0
  :ensure magit
  :config (progn
	    (evil-set-initial-state 'magit-mode 'normal)
	    (evil-set-initial-state 'magit-status-mode 'normal)
	    (evil-set-initial-state 'magit-diff-mode 'normal)
	    (evil-set-initial-state 'magit-log-mode 'normal)))

;; set dired
(use-package dired
  :ensure nil
  :commands (dired dired-jump)
  :bind (("C-x Cj" . dired-jump))
  :config (evil-collection-define-key 'normal 'dired-mode-map "h" 'dired-up-directory "l" 'dired-find-file))

(use-package all-the-icons-dired
  :ensure t
  :hook (dired-mode . all-the-icons-dired-mode))

;; yasnippet
(defun company-yasnipper-or-completion ()
  "Solve comp=any yasnippet conflicts."
  (interactive)
  (let ((yas-fallback-behavior
	  (apply 'company-complete-common nil)))
    (yas-expand)))

(add-hook 'company-mode-hook
	  (lambda ()
	    (substitute-key-definition
	     'company-complete-common
	     'company-yasnippet-or-completion
	     company-active-map)))

(use-package yasnippet
  :ensure t
  :defer t
  :config (add-to-list 'yas-snippet-dirs "~/.emacs.d/snippets") (yas-global-mode 1)
  :diminish yas-global-mode)

(use-package yasnippet-snippets :ensure t :after yasnippet)

(defun bb/org-latex-yas ()
  "Activate org and LaTeX yas expansion in org-mode buffers."
  (yas-minor-mode)
  (yas-activate-extra-mode 'latex-mode))

(add-hook 'org-mode-hook #'bb/org-latex-yas)

;; set avy jumps
(use-package avy :ensure t :defer t
  :bind (("C-c ;" . avy-goto-char-2)
	 ("C-c p" . avy-goto-line)))

(use-package flycheck
  :ensure t
  :config
  (add-hook 'after-init-hook #'global-flycheck-mode))

;; set Eglot lsp client
(use-package eglot
  :ensure t
  ;:hook ((prog-mode . eglot-ensure)(pyvenv-workon . eglot))
  :hook ((prog-mode . eglot-ensure))
  :defer t

  :config (add-to-list 'eglot-server-programs '(python-mode . ("pylsp")))
  )

(use-package eglot-booster
	:after eglot
	:config	(eglot-booster-mode))


(global-set-key (kbd "C-,") 'xref-go-back)
(global-set-key (kbd "C-.") 'xref-goto-xref)


(use-package consult-eglot :ensure t :after eglot)
(use-package flycheck-eglot :ensure t :after eglot)
(use-package auto-complete :ensure t :init (ac-config-default))

;; markdown mode
(use-package markdown-mode
  :ensure t
  :defer t
  :config (progn
	    (push '("\\.text\\'" . markdown-mode) auto-mode-alist)
	    (push '("\\.markdown\\'" . markdown-mode) auto-mode-alist)
	    (push '("\\.md\\'" . markdown-mode) auto-mode-alist)))

;; csv mode
(use-package csv-mode
  :ensure t
  :defer t
  :mode ("\\.\\(csv\\|tsv\\)\\'"))

;; yaml mode
(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))
(use-package yaml-mode :defer t)

;; dockerfile mode
(use-package dockerfile-mode
  :ensure t)

;; haskell mode
(use-package haskell-mode
  :ensure t
  :config
  (add-hook 'haskell-mode-hook 'interactive-haskell-mode))

;(use-package pyvenv :ensure t :defer t)
;(setenv "WORKON_HOME" "/home/barry/.cache/pypoetry/virtualenvs")

(use-package python-black
  :ensure t
  :demand t
  :after python
  :hook ((python-mode . python-black-on-save-mode)))

(use-package poetry ; PyPoetry support
  :ensure t
  :defer t
  ;:hook (python-mode . poetry-tracking-mode)
)

(setq poetry-tracking-strategy 'switch-buffer)

(use-package olivetti
  :ensure t
  :init (setq oliivetti-body-width .70))

(add-hook 'org-mode-hook #'smartparens-mode)
(add-hook 'org-mode-hook 'olivetti-mode)

(defun bb/org-mode-setup ()
  (org-indent-mode)
  (visual-line-mode 1)
  (setq evil-auto-indent nil))

(defun bb/org-font-setup ()
  (font-lock-add-keywords 'org-mode '(("^ *\\([-]\\) " (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))
  ; set faces for heading levels
  (dolist (face '((org-level-1 . 1.2)
		  (org-level-2 . 1.1)
		  (org-level-3 . 1.05)
		  (org-level-4 . 1.0)
		  (org-level-5 . 1.1)
		  (org-level-6 . 1.1)
		  (org-level-7 . 1.1)
		  (org-level-8 . 1.1)))
    (set-face-attribute 'default nil :font "Source Code Pro" :height 140)
    (set-face-attribute (car face) nil :font "Source Code Pro" :height (cdr face)))
    ; ensure that everything that should be fixed-pitch in org files appears that way
    (set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
    (set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch))
    (set-face-attribute 'org-table nil   :inherit '(shadow fixed-pitch))
    (set-face-attribute 'org-formula nil :inherit 'fixed-pitch)
    (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
    (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
    (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
    (set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch)
    (set-face-attribute 'line-number nil :inherit 'fixed-pitch)
    (set-face-attribute 'line-number-current-line nil :inherit 'fixed-pitch))


(defun bb/setup-buffer-face ()
  (setq buffer-face-mode-face '(:family "Source Code Pro 14"))
  (buffer-face-mode))

(add-hook 'org-agenda-mode-hook 'bb/setup-buffer-face)

(defun bb/after-org-mode-load ()
  (visual-line-mode)
  (vi-tilde-fringe-mode -1)
  (require 'org-indent)
  (org-indent-mode)
  (set-face-attribute 'org-indent nil :inherit '(org-hide fixed-pitch))
  (variable-pitch-mode 1)
  (require 'olivetti)
  (olivetti-mode))

(use-package org
  :hook (org-mode . bb/org-mode-setup)
  :config
  (setq org-ellipsis "  \u25be" org-hide-emphasis-markers t)
  (setq org-agenda-start-with-log-mode t)
  (setq org-log-done 'time)
  (setq org-log-into-drawer t)
  (setq org-startup-folded t)
  (setq org-agenda-files (list "~/org/agenda"))
  (setq org-todo-keywords '((sequence "TODO(t)" "URGENT(u)" "TOFIX(f)" "DOING(d)" "|" "DONE(o!)")
			    (sequence "BACKLOG(b)" "PLANNED(p)" "RESEARCH(r)" "REVIEW(v)" "|" "COMPLETED(c)" "CANCELLED(k@)")))
  (setq org-export-backends '(md))
  (setq org-src-preserve-indentation t)
  (bb/org-font-setup))

(global-set-key (kbd "C-x a") 'org-agenda)

;;; Org babel
(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (python . t)))

(setq org-confirm-babel-evaluate nil)

(require 'org-tempo)
(add-to-list 'org-structure-template-alist '("sh" . "src shell"))
(add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
(add-to-list 'org-structure-template-alist '("py" . "src python"))

; Org bullets
(use-package org-bullets
  :after org
  :hook (org-mode . org-bullets-mode)
  :custom (org-bullets-bullet-list '("◉" "○" "●" "○○" "●●" "○○○" "●●●")))

; autolist
(use-package org-autolist)
(add-hook 'org-mode-hook (lambda () (org-autolist-mode)))

;; ORG roam
(use-package org-roam
  :ensure t
  :custom
  (org-roam-directory "~/org/notes")
  (org-roam-completion-everywhere t)
  (org-roam-capture-templates
   '(("d" "default" plain
      "\n%?"
      :if-new (file+head "${slug}-%<%Y%m%d%H%H%S>.org" "#+STARTUP:latexpreview\n#+STARTUP:inlineimages\n #+date: %U\n #+title:${title}\n backlink:\n tags:\n* ${title}\n")
      :unnarrowed t)))
  :bind (("C-x n l" . org-roam-buffer-toggle)
	 ("C-x n f" . org-roam-node-find)
	 ("C-x n i" . org-roam-node-insert)
	 ;:map org-mode-map ("C-M-i" . completion-at-point)
	 )

  :config (org-roam-setup))

(org-roam-db-autosync-mode)

;; ORG roam
(use-package org-roam
  :ensure t
  :custom
  (org-roam-directory "~/org/notes")
  (org-roam-completion-everywhere t)
  (org-roam-capture-templates
   '(("d" "default" plain
      "\n%?"
      :if-new (file+head "${slug}-%<%Y%m%d%H%H%S>.org" "#+STARTUP:latexpreview\n#+STARTUP:inlineimages\n #+date: %U\n #+title:${title}\n backlink:\n tags:\n* ${title}\n")
      :unnarrowed t)))
  :bind (("C-x n l" . org-roam-buffer-toggle)
	 ("C-x n f" . org-roam-node-find)
	 ("C-x n i" . org-roam-node-insert)
	 ;:map org-mode-map ("C-M-i" . completion-at-point)
	 )

  :config (org-roam-setup))

(org-roam-db-autosync-mode)

; latex
(use-package org-fragtog
  :hook (org-mode . org-fragtog-mode))
(setq org-format-latex-options (plist-put org-format-latex-options :scale 1.6))

(setenv "PATH" (concat "/home/barry/.local/bin:" (getenv "PATH")))
(setq exec-path (append exec-path '("/home/barry/.local/bin")))

;; general bindings
(global-set-key (kbd "C-<tab>") 'other-window)

(use-package realgud
  :ensure t
  :defer t
 )

;; set gb back to normal for memory reasons
(setq gc-cons-threshold (* 50 1024 1024))
