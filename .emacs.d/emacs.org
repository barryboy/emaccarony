#+PROPERTY: header-args :tangle init.el
#+TITLE: Barry's Emaccarony

* Emacs Config V6
Welcome to me config.
** Init Stuff
*** Garbage Collection & Startup Profiling
- Reduce Garbage Collector calls during startup.
- Record Startup Time
#+begin_src emacs-lisp

;; lower gc calls
(setq gc-cons-threshold (* 50 1024 1024))
;; profile emacs startup
(add-hook 'emacs-startup-hook
	  (lambda ()
	    (message "*** Emacs loaded in %s seconds with %d garbage collections" (emacs-init-time "%.2f") gcs-done)))

#+end_src
*** Package Manager
- To install all sorta stuff
- use-package
#+begin_src emacs-lisp

;; package manager
(require 'package)
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
             ("org" . "https://orgmode.org/elpa/")
	     ("elpa" . "https://elpa.gnu.org/packages/")))
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))


;; use-package
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

#+end_src
*** AutoSave
- Change autosave directory -> no more .#files in every directory emacs touches
#+begin_src emacs-lisp

;; autosave
(setq backup-directory-alist '(("." . "~/.emacs.d/backup"))
      backup-by-copying t    ; Don't delink hardlinks
      version-control t      ; Use version numbers on backups
      delete-old-versions t  ; Automatically delete excess backups
      kept-new-versions 20   ; How many of the newest versions to keep
      kept-old-versions 5)   ; And how many of the old

#+end_src
** Aesthethiccs
*** Font
- Source Code Pro
#+begin_src emacs-lisp

;; set fonts
(set-face-attribute 'default nil :font "Source Code Pro" :height 120)
(set-face-attribute 'fixed-pitch nil :font "Source Code Pro" :height 120)
(set-face-attribute 'variable-pitch nil :font "Source Code Pro" :height 140)
(set-frame-font "Source Code Pro 12")
(setq default-frame-alist '((font . "Source Code Pro 12")))

#+end_src
*** Toolbars
- Disable Default Toolbars
- Doom Modeline
#+begin_src emacs-lisp

;; disable toolbars
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)
(setq frame-resize-pixelwise t)

;; set modeline
(setq doom-modeline-support-imenu t)
(setq doom-modeline-height 25)
(use-package doom-modeline
  :ensure t
  :init
    (doom-modeline-mode 1)
  :custom ((doom-modeline-height 25)))

#+end_src
*** Line Number
- Relative line number
- Absolute line number shown on current line
- Color highlights
#+begin_src emacs-lisp

; set line number
(use-package linum-relative
  :ensure t
  :config
  (linum-relative-global-mode 1)
  (setq linum-relative-current-symbol "")
  (setq linum-relative-format "%4s \u2502"))

(defun bb/absolute-line-number ()
  (if (and (not (minibufferp))
           (eq (line-number-at-pos) linum-relative-last-pos))
      (format "%4s \u2502" (line-number-at-pos)) ""))

(add-hook 'linum-before-numbering-hook #'bb/absolute-line-number)

(add-hook 'dashboard-after-initialize-hook
          (lambda ()
            (with-current-buffer "*dashboard*"
              (linum-relative-mode -1))))

#+end_src
*** Theme
- Doom Themes
#+begin_src emacs-lisp

;; Trust all themes, unsecure but you know what ur doing
(setq custom-safe-themes t)
;; set theme
(use-package doom-themes
  :ensure t
  :config
  (setq doom-themes-enable-bold t)
  (setq	doom-themes-enable-italic t)
  (load-theme 'doom-laserwave)
  ;(load-theme 'doom-shades-of-purple)
)
#+end_src
*** Icons
- All of The Icons
#+begin_src emacs-lisp

;; set icons
(use-package all-the-icons
  :ensure t)

;; emojis
(use-package emojify
  :ensure t
  :defer 0)

#+end_src
*** Transparency
- Transparent window go brr
#+begin_src emacs-lisp

;; set transparency
(set-frame-parameter (selected-frame) 'alpha '(90 . 85))
(add-to-list 'default-frame-alist '(alpha . (90 . 85)))
(set-frame-parameter (selected-frame) 'fullscreen 'maximized)
(add-to-list 'default-frame-alist '(fullscreen . maximized))

#+end_src
*** Dashboard
- Fancy Welcome Screen to showcase your Autism
- hella code so that it works both when started in daemon mode or not
#+begin_src emacs-lisp

(if (daemonp)
    (add-hook 'after-make-frame-functions
              (lambda (frame)
                (with-selected-frame frame
                  (when (display-graphic-p)
                    (use-package dashboard
                      :ensure t
                      :config
                      (dashboard-setup-startup-hook)
                      (setq dashboard-items '((bookmarks . 10)
                                              (recents . 5)
                                              (projects . 2)
                                              (agenda . 2)))
                      (setq dashboard-banner-logo-title "Welcome Barry!")
                      (setq dashboard-startup-banner "~/Pictures/barrybbenson.gif")
                      (setq dashboard-center-content t)
                      (setq dashboard-show-shortcuts t)
                      (setq initial-buffer-choice
                            (lambda ()
                              (if (get-buffer "*dashboard*")
                                  (get-buffer "*dashboard*")
                                (progn
                                  (dashboard-refresh-buffer)
                                  (get-buffer "*dashboard*")))))
                      (setq dashboard-set-heading-icons t)
                      (setq dashboard-set-navigator t)
                      (dashboard-insert-shortcut (dashboard-get-shortcut 'recents) "f" "Recent Files:")
                      (dashboard-insert-shortcut (dashboard-get-shortcut 'agenda) "a" "Agenda for today:"))))))
  ;; For non-daemon mode, set up normally
  (use-package dashboard
    :ensure t
    :config
    (dashboard-setup-startup-hook)
    (setq dashboard-items '((bookmarks . 10)
                            (recents . 5)
                            (projects . 2)
                            (agenda . 2)))
    (setq dashboard-banner-logo-title "Welcome Barry!")
    (setq dashboard-startup-banner "~/Pictures/barrybbenson.gif")
    (setq dashboard-center-content t)
    (setq dashboard-show-shortcuts t)
    (setq initial-buffer-choice
          (lambda ()
            (if (get-buffer "*dashboard*")
                (get-buffer "*dashboard*")
              (progn
                (dashboard-refresh-buffer)
                (get-buffer "*dashboard*")))))
    (setq dashboard-set-heading-icons t)
    (setq dashboard-set-navigator t)
    (dashboard-insert-shortcut (dashboard-get-shortcut 'recents) "f" "Recent Files:")
    (dashboard-insert-shortcut (dashboard-get-shortcut 'agenda) "a" "Agenda for today:")))

#+end_src
** Programming
*** Evil Mode
- VIM but in Emacs
#+begin_src emacs-lisp

;; set evil
(use-package evil
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  (setq evil-want-C-i-jump nil)
  :config
  (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join))

(evil-global-set-key 'motion "j" 'evil-next-visual-line)
(evil-global-set-key 'motion "k" 'evil-previous-visual-line)
(evil-set-initial-state 'messages-buffer-mode 'normal)
(evil-set-initial-state 'dashboard-mode 'normal)
(evil-set-undo-system 'undo-redo)
(define-key evil-normal-state-map (kbd "C-h") 'evil-window-left)
(define-key evil-normal-state-map (kbd "C-j") 'evil-window-down)
(define-key evil-normal-state-map (kbd "C-k") 'evil-window-up)
(define-key evil-normal-state-map (kbd "C-l") 'evil-window-right)

(use-package evil-collection
  :ensure t
  :after evil
  :config (evil-collection-init))

#+end_src
*** Misc
- Save Cursor position on files
- Esc behaves as quit
- yes-or-no -> y-or-n prompt
- Nice Indentation
- simple X clip copy-paste
- C-a jumps at start of line
- Delete trailing whitespaces on file save
- save history
- cycle tabs with C-<tab>
#+begin_src emacs-lisp

;; small misc
(save-place-mode 1)
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)
(defalias 'yes-or-no-p 'y-or-n-p)
(use-package hindent :ensure t :defer 0)
(use-package simpleclip :ensure t :config (simpleclip-mode 1))
(use-package crux :ensure t :bind (("C-a" . crux-move-beginning-of-line)))
(add-hook 'before-save-hook 'delete-trailing-whitespace)
(use-package savehist :ensure t :init (savehist-mode))
;; general bindings
(global-set-key (kbd "C-<tab>") 'other-window)

#+end_src
*** Smart Parenthesis
- Autocomplete parenthesis
- More movement keys
- R A I N B O W  Parenthesis coloring
#+begin_src emacs-lisp

;; set parenthesis behaviour
(use-package smartparens
  :ensure t
  :config (add-hook 'prog-mode-hook 'smartparens-mode))

(bind-keys
   :map smartparens-mode-map
     ("C-<down>" . sp-down-sexp)
     ("C-<up>" . sp-up-sexp))

(use-package rainbow-delimiters
  :ensure t)

#+end_src
*** MiniBuffer Completion
- Vertical candidates minibuffer completion
- Candidates Annotations
- Orderless backend
#+begin_src emacs-lisp

;; set minibuf autocomplete
(use-package vertico
  :ensure t
  :bind (:map vertico-map
	      ("C-j" . vertico-next)
	      ("C-k" . vertico-previous)
	      ("C-f" . vertico-exit)
	      :map minibuffer-local-map
	      ("M-h" . backward-kill-word))
  :custom (vertico-cycle t)
  :init (vertico-mode))

(define-key vertico-map "?" #'minibuffer-completion-help) ; autocomplete with tab
(define-key vertico-map (kbd "M-RET") #'minibuffer-force-complete-and-exit)
(define-key vertico-map (kbd "M-TAB") #'minibuffer-complete)

(use-package marginalia
  :after vertico
  :ensure t
  :bind (("M-A" . marginalia-cycle)
	 :map minibuffer-local-map
	 ("M-A" . marginalia-cycle))
  :init (marginalia-mode))

(use-package orderless
  :ensure t
  :custom
  (completion-styles '(orderless basic))
  (completion-category-overrides '((file (style basic partial-complete)))))

#+end_src
*** Recent Files
- recent files completion
#+begin_src emacs-lisp

;; set recent file mode
(recentf-mode 1)
(setq recentf-max-menu-items 20)
(setq recent-max-saved-items 20)
(run-at-time nil (* 5 60) 'recentf-save-list)

(defun recentf-open-files-compl ()
  (interactive)
  (let* ((tocpl (mapcar (lambda (x) (cons (file-name-nondirectory x) x))
			recentf-list))
	 (fname (completing-read "File name: " tocpl nil nil)))
    (when fname (find-file (cdr (assoc-string fname tocpl))))))

#+end_src
*** Magit
- Handle all your GIT business without leaving emacs
#+begin_src emacs-lisp

;; set magit git client
(use-package magit
  :defer 0
  :ensure magit
  :config (progn
	    (evil-set-initial-state 'magit-mode 'normal)
	    (evil-set-initial-state 'magit-status-mode 'normal)
	    (evil-set-initial-state 'magit-diff-mode 'normal)
	    (evil-set-initial-state 'magit-log-mode 'normal)))

#+end_src
*** Dired
- Move, Rename, Copy files...
- Any filesystem exploration
#+begin_src emacs-lisp

;; set dired
(use-package dired
  :ensure nil
  :commands (dired dired-jump)
  :bind (("C-x Cj" . dired-jump))
  :config (evil-collection-define-key 'normal 'dired-mode-map "h" 'dired-up-directory "l" 'dired-find-file))

(use-package all-the-icons-dired
  :ensure t
  :hook (dired-mode . all-the-icons-dired-mode))

#+end_src
*** Snippetz
- Autocomplete snippets
#+begin_src emacs-lisp

;; yasnippet
(defun company-yasnipper-or-completion ()
  "Solve comp=any yasnippet conflicts."
  (interactive)
  (let ((yas-fallback-behavior
	  (apply 'company-complete-common nil)))
    (yas-expand)))

(add-hook 'company-mode-hook
	  (lambda ()
	    (substitute-key-definition
	     'company-complete-common
	     'company-yasnippet-or-completion
	     company-active-map)))

(use-package yasnippet
  :ensure t
  :defer t
  :config (add-to-list 'yas-snippet-dirs "~/.emacs.d/snippets") (yas-global-mode 1)
  :diminish yas-global-mode)

(use-package yasnippet-snippets :ensure t :after yasnippet)

(defun bb/org-latex-yas ()
  "Activate org and LaTeX yas expansion in org-mode buffers."
  (yas-minor-mode)
  (yas-activate-extra-mode 'latex-mode))

(add-hook 'org-mode-hook #'bb/org-latex-yas)

#+end_src
*** More Movement
- cool movement keys with avy
#+begin_src emacs-lisp

;; set avy jumps
(use-package avy :ensure t :defer t
  :bind (("C-c ;" . avy-goto-char-2)
	 ("C-c p" . avy-goto-line)))

#+end_src
** IDE
*** Syntax Checking
- check syntax for 40+ programming languages
#+begin_src emacs-lisp

(use-package flycheck
  :ensure t
  :config
  (add-hook 'after-init-hook #'global-flycheck-mode))

#+end_src
*** LSP client & AutoComplete
- eglot -> lightweight baby
- autocomplete box inside the buffer
#+begin_src emacs-lisp

;; set Eglot lsp client
(use-package eglot
  :ensure t
  ;:hook ((prog-mode . eglot-ensure)(pyvenv-workon . eglot))
  :hook ((prog-mode . eglot-ensure))
  :defer t

  :config (add-to-list 'eglot-server-programs '(python-mode . ("pylsp")))
  )

(use-package eglot-booster
	:after eglot
	:config	(eglot-booster-mode))


(global-set-key (kbd "C-,") 'xref-go-back)
(global-set-key (kbd "C-.") 'xref-goto-xref)


(use-package consult-eglot :ensure t :after eglot)
(use-package flycheck-eglot :ensure t :after eglot)
(use-package auto-complete :ensure t :init (ac-config-default))

#+end_src
*** Markdown Mode
- utils for .md files
#+begin_src emacs-lisp

;; markdown mode
(use-package markdown-mode
  :ensure t
  :defer t
  :config (progn
	    (push '("\\.text\\'" . markdown-mode) auto-mode-alist)
	    (push '("\\.markdown\\'" . markdown-mode) auto-mode-alist)
	    (push '("\\.md\\'" . markdown-mode) auto-mode-alist)))

#+end_src
*** CSV Mode
- utils to view csv files more as a table
- still kinda sucks tho
#+begin_src emacs-lisp

;; csv mode
(use-package csv-mode
  :ensure t
  :defer t
  :mode ("\\.\\(csv\\|tsv\\)\\'"))

#+end_src
*** YAML Mode
- .yaml utilities
#+begin_src emacs-lisp

;; yaml mode
(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))
(use-package yaml-mode :defer t)

#+end_src
*** Dockerfile Mode
- Dockerfile utilities, kinda nice
#+begin_src emacs-lisp

;; dockerfile mode
(use-package dockerfile-mode
  :ensure t)

#+end_src
*** Haskell Mode
- haskell LSP server
- Interactive Completion & shit
#+begin_src emacs-lisp

;; haskell mode
(use-package haskell-mode
  :ensure t
  :config
  (add-hook 'haskell-mode-hook 'interactive-haskell-mode))

#+end_src
*** Python Stuff
Because choosing python LSP is so simple
So many good options
Suuper Fastt
(all irony btw)
- Pyright: bloat slow mess, no gotos... c'mon
- Jedi: bloat slow mess that attaches itself to local venv so hard to track gotojumps of local libs
- pylsp: meh
#+begin_src emacs-lisp

;(use-package pyvenv :ensure t :defer t)
;(setenv "WORKON_HOME" "/home/barry/.cache/pypoetry/virtualenvs")

#+end_src
Python black formatter
#+begin_src emacs-lisp

(use-package python-black
  :ensure t
  :demand t
  :after python
  :hook ((python-mode . python-black-on-save-mode)))

#+end_src
*** Poetry Mode
- maybe the best way to handle python environments - LSP in emacs
#+begin_src emacs-lisp

(use-package poetry ; PyPoetry support
  :ensure t
  :defer t
  ;:hook (python-mode . poetry-tracking-mode)
)

(setq poetry-tracking-strategy 'switch-buffer)


#+end_src
** ORG Mode
*** Center Buffers
Olivetti to have borders around org buffers, ez reading
#+begin_src emacs-lisp

(use-package olivetti
  :ensure t
  :init (setq oliivetti-body-width .70))

#+end_src

*** ORG Setup
- activate smartparens in org buffers
- activate olivetti-mode
#+begin_src emacs-lisp

(add-hook 'org-mode-hook #'smartparens-mode)
(add-hook 'org-mode-hook 'olivetti-mode)

(defun bb/org-mode-setup ()
  (org-indent-mode)
  (visual-line-mode 1)
  (setq evil-auto-indent nil))

#+end_src
*** Font Setup
- varied size fonts + dumb shit
#+begin_src emacs-lisp

(defun bb/org-font-setup ()
  (font-lock-add-keywords 'org-mode '(("^ *\\([-]\\) " (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))
  ; set faces for heading levels
  (dolist (face '((org-level-1 . 1.2)
		  (org-level-2 . 1.1)
		  (org-level-3 . 1.05)
		  (org-level-4 . 1.0)
		  (org-level-5 . 1.1)
		  (org-level-6 . 1.1)
		  (org-level-7 . 1.1)
		  (org-level-8 . 1.1)))
    (set-face-attribute 'default nil :font "Source Code Pro" :height 140)
    (set-face-attribute (car face) nil :font "Source Code Pro" :height (cdr face)))
    ; ensure that everything that should be fixed-pitch in org files appears that way
    (set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
    (set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch))
    (set-face-attribute 'org-table nil   :inherit '(shadow fixed-pitch))
    (set-face-attribute 'org-formula nil :inherit 'fixed-pitch)
    (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
    (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
    (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
    (set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch)
    (set-face-attribute 'line-number nil :inherit 'fixed-pitch)
    (set-face-attribute 'line-number-current-line nil :inherit 'fixed-pitch))


(defun bb/setup-buffer-face ()
  (setq buffer-face-mode-face '(:family "Source Code Pro 14"))
  (buffer-face-mode))

(add-hook 'org-agenda-mode-hook 'bb/setup-buffer-face)

#+end_src

*** After Setup Hooks
- much shit you shouldnt worry about
#+begin_src emacs-lisp

(defun bb/after-org-mode-load ()
  (visual-line-mode)
  (vi-tilde-fringe-mode -1)
  (require 'org-indent)
  (org-indent-mode)
  (set-face-attribute 'org-indent nil :inherit '(org-hide fixed-pitch))
  (variable-pitch-mode 1)
  (require 'olivetti)
  (olivetti-mode))

#+end_src
*** Main ORG setup
- call hooks
- agenda setup
- TODO setup
- export backends
#+begin_src emacs-lisp

(use-package org
  :hook (org-mode . bb/org-mode-setup)
  :config
  (setq org-ellipsis "  \u25be" org-hide-emphasis-markers t)
  (setq org-agenda-start-with-log-mode t)
  (setq org-log-done 'time)
  (setq org-log-into-drawer t)
  (setq org-startup-folded t)
  (setq org-agenda-files (list "~/org/agenda"))
  (setq org-todo-keywords '((sequence "TODO(t)" "URGENT(u)" "TOFIX(f)" "DOING(d)" "|" "DONE(o!)")
			    (sequence "BACKLOG(b)" "PLANNED(p)" "RESEARCH(r)" "REVIEW(v)" "|" "COMPLETED(c)" "CANCELLED(k@)")))
  (setq org-export-backends '(md))
  (setq org-src-preserve-indentation t)
  (bb/org-font-setup))

(global-set-key (kbd "C-x a") 'org-agenda)

#+end_src
*** ORG Babel
Jupyter Notebooks but for any prog lang!! (why is this so rare?)
#+begin_src emacs-lisp

;;; Org babel
(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (python . t)))

(setq org-confirm-babel-evaluate nil)

(require 'org-tempo)
(add-to-list 'org-structure-template-alist '("sh" . "src shell"))
(add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
(add-to-list 'org-structure-template-alist '("py" . "src python"))

#+end_src
*** ORG Bullets
- cool looking bullets
- autolist
#+begin_src emacs-lisp

; Org bullets
(use-package org-bullets
  :after org
  :hook (org-mode . org-bullets-mode)
  :custom (org-bullets-bullet-list '("◉" "○" "●" "○○" "●●" "○○○" "●●●")))

; autolist
(use-package org-autolist)
(add-hook 'org-mode-hook (lambda () (org-autolist-mode)))

#+end_src
*** ORG Roam
- Obsidian but plain better,
#+begin_src emacs-lisp

;; ORG roam
(use-package org-roam
  :ensure t
  :custom
  (org-roam-directory "~/org/notes")
  (org-roam-completion-everywhere t)
  (org-roam-capture-templates
   '(("d" "default" plain
      "\n%?"
      :if-new (file+head "${slug}-%<%Y%m%d%H%H%S>.org" "#+STARTUP:latexpreview\n#+STARTUP:inlineimages\n #+date: %U\n #+title:${title}\n backlink:\n tags:\n* ${title}\n")
      :unnarrowed t)))
  :bind (("C-x n l" . org-roam-buffer-toggle)
	 ("C-x n f" . org-roam-node-find)
	 ("C-x n i" . org-roam-node-insert)
	 ;:map org-mode-map ("C-M-i" . completion-at-point)
	 )

  :config (org-roam-setup))

(org-roam-db-autosync-mode)

#+end_src
*** ORG Roam UI
show on web browser the graph view of the notes
#+begin_src emacs-lisp

;; ORG roam
(use-package org-roam
  :ensure t
  :custom
  (org-roam-directory "~/org/notes")
  (org-roam-completion-everywhere t)
  (org-roam-capture-templates
   '(("d" "default" plain
      "\n%?"
      :if-new (file+head "${slug}-%<%Y%m%d%H%H%S>.org" "#+STARTUP:latexpreview\n#+STARTUP:inlineimages\n #+date: %U\n #+title:${title}\n backlink:\n tags:\n* ${title}\n")
      :unnarrowed t)))
  :bind (("C-x n l" . org-roam-buffer-toggle)
	 ("C-x n f" . org-roam-node-find)
	 ("C-x n i" . org-roam-node-insert)
	 ;:map org-mode-map ("C-M-i" . completion-at-point)
	 )

  :config (org-roam-setup))

(org-roam-db-autosync-mode)

#+end_src
*** ORG Latex Fragment
render latex fragment
#+begin_src emacs-lisp

; latex
(use-package org-fragtog
  :hook (org-mode . org-fragtog-mode))
(setq org-format-latex-options (plist-put org-format-latex-options :scale 1.6))

(setenv "PATH" (concat "/home/barry/.local/bin:" (getenv "PATH")))
(setq exec-path (append exec-path '("/home/barry/.local/bin")))

#+end_src
** Misc
*** Other Keybinding
#+begin_src emacs-lisp

;; general bindings
(global-set-key (kbd "C-<tab>") 'other-window)


#+end_src
*** Nice Debugger
RealGUD debugger
#+begin_src emacs-lisp

(use-package realgud
  :ensure t
  :defer t
 )

#+end_src
*** Reset GC
#+begin_src emacs-lisp

;; set gb back to normal for memory reasons
(setq gc-cons-threshold (* 50 1024 1024))

#+end_src
